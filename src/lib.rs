//! Code to support calling into C APIs which consume strings.

#![no_std]
#![warn(missing_docs)]

extern crate alloc;

use alloc::{format, string::String};
use core::slice::from_raw_parts;
use rcstring::{CString, Error};

#[inline]
/// Calls the given function with a lifetime-bound `CString` constructed from
/// the given `String`.
pub fn with_cstring<T, E: From<Error>>(
    s: String,
    f: impl FnOnce(CString<'_>) -> Result<T, E>,
) -> Result<T, E> {
    let string = format!("{}\0", s);
    f(CString::new(&string)?)
}

#[inline]
/// Creates an owned `String` from a given `CString`.
pub fn from_cstring(cstring: &CString<'_>) -> String {
    unsafe { from_cstring_raw(cstring.into_raw()) }
}

/// Creates an owned `String` from a given raw C string pointer.
///
/// # Safety
/// This function assumes that the string is a valid C string with a null
/// terminator. Passing a pointer which does not refer to such a string will
/// result in undefined behaviour.
///
/// The function does not take responsibility for the C string after reading
/// from it; managing allocation and lifetime is up to the caller.
pub unsafe fn from_cstring_raw(cstring: *const libc::c_char) -> String {
    String::from_utf8_lossy(from_raw_parts(cstring as *const u8, libc::strlen(cstring))).into()
}
